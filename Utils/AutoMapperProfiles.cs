using AutoMapper;
using TCATest.API.Dtos;
using TCATest.API.Models;

namespace TCATest.API.Utils
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            // student maps
            CreateMap<Student, StudentForListDto>();

            CreateMap<Student, StudentForDetailDto>();

            CreateMap<Student, StudentForPostDto>();

            CreateMap<Student, StudentForPutDto>();

            // email maps
            CreateMap<Email, EmailForListDto>();

            CreateMap<Email, EmailForDetailDto>();

            CreateMap<Email, EmailForPostDto>();

            CreateMap<Email, EmailForPutDto>();

            // phone maps
            CreateMap<Phone, PhoneForListDto>();

            CreateMap<Phone, PhoneForDetailDto>();

            CreateMap<Phone, PhoneForPostDto>();

            CreateMap<Phone, PhoneForPutDto>();

            // address maps
            CreateMap<Address, AddressForListDto>();

            CreateMap<Address, AddressForDetailDto>();

            CreateMap<Address, AddressForPostDto>();

            CreateMap<Address, AddressForPutDto>();
        }
    }
}