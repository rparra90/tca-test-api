namespace TCATest.API.Utils
{
    public class ApiMessages
    {
        public static readonly string NOT_FOUND = "Item not found";
        public static readonly string ITEM_DELETED = "Item deleted";
        public static readonly string WRONG_ENUM_TYPE = "Wrong enum type";
        public static readonly string INVALID_BODY = "Invalid body";
    }
}