using System.Collections.Generic;
using System.Threading.Tasks;
using TCATest.API.Models;

namespace TCATest.API.Data
{
    public interface IEmailRepository
    {
        void Add<T>(T entity) where T : class;
        void Delete<T>(T entity) where T : class;
        Task<bool> SaveAll();
        Task<IEnumerable<Email>> GetEmails();
        Task<IEnumerable<Email>> GetEmailsWhereStudent(int id);
        Task<Email> GetEmail(int id);
    }
}