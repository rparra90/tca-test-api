using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TCATest.API.Models;

namespace TCATest.API.Data
{
    public class AddressRepository : IAddressRepository
    {
        private readonly DataContext context;

        public AddressRepository(DataContext context)
        {
            this.context = context;
        }

        public void Add<T>(T entity) where T : class
        {
            this.context.Add(entity);
        }

        public void Delete<T>(T entity) where T : class
        {
            this.context.Remove(entity);
        }

        public async Task<Address> GetAddress(int id)
        {
            var address = await this.context.Addresses.FirstOrDefaultAsync(x => x.Id == id);
            return address;
        }

        public async Task<IEnumerable<Address>> GetAddresses()
        {
            var addresses = await this.context.Addresses.ToListAsync();
            return addresses;
        }

        public async Task<IEnumerable<Address>> GetAddressesWhereStudent(int id)
        {
            var addresses = await this.context.Addresses.Where(x => x.StudentId == id).ToListAsync();
            return addresses;
        }

        public async Task<bool> SaveAll()
        {
            return await this.context.SaveChangesAsync() > 0;
        }
    }
}