using System.Collections.Generic;
using System.Threading.Tasks;
using TCATest.API.Models;

namespace TCATest.API.Data
{
    public interface IPhoneRepository
    {
        void Add<T>(T entity) where T : class;
        void Delete<T>(T entity) where T : class;
        Task<bool> SaveAll();
        Task<IEnumerable<Phone>> GetPhones();
        Task<IEnumerable<Phone>> GetPhonesWhereStudent(int id);
        Task<Phone> GetPhone(int id);
    }
}