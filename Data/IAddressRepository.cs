using System.Collections.Generic;
using System.Threading.Tasks;
using TCATest.API.Models;

namespace TCATest.API.Data
{
    public interface IAddressRepository
    {
         void Add<T>(T entity) where T: class;
         void Delete<T>(T entity) where T: class;
         Task<bool> SaveAll();
         Task<IEnumerable<Address>> GetAddresses();
         Task<IEnumerable<Address>> GetAddressesWhereStudent(int id);
         Task<Address> GetAddress(int id);
    }
}