using Microsoft.EntityFrameworkCore;
using TCATest.API.Models;

namespace TCATest.API.Data
{
    public class DataContext : DbContext
    {

        public DbSet<Student> Students { get; set; }
        public DbSet<Email> Emails { get; set; }
        public DbSet<Phone> Phones { get; set; }
        public DbSet<Address> Addresses { get; set; }

        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
            // ...
        }
    }
}