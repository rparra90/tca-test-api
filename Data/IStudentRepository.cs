using System.Collections.Generic;
using System.Threading.Tasks;
using TCATest.API.Models;

namespace TCATest.API.Data
{
    public interface IStudentRepository
    {
        void Add<T>(T entity) where T : class;
        void Delete<T>(T entity) where T : class;
        Task<bool> SaveAll();
        Task<IEnumerable<Student>> GetStudents();
        Task<Student> GetStudent(int id);

    }
}