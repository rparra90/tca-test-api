using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TCATest.API.Models;

namespace TCATest.API.Data
{
    public class PhoneRepository : IPhoneRepository
    {
        private readonly DataContext context;

        public PhoneRepository(DataContext context)
        {
            this.context = context;
        }

        public void Add<T>(T entity) where T : class
        {
            this.context.Add(entity);
        }

        public void Delete<T>(T entity) where T : class
        {
            this.context.Remove(entity);
        }

        public async Task<Phone> GetPhone(int id)
        {
            var phone = await this.context.Phones.FirstOrDefaultAsync();
            return phone;
        }

        public async Task<IEnumerable<Phone>> GetPhones()
        {
            var phones = await this.context.Phones.ToListAsync();
            return phones;
        }

        public async Task<IEnumerable<Phone>> GetPhonesWhereStudent(int id)
        {
            var phones = await this.context.Phones.Where(x => x.StudentId == id).ToListAsync();
            return phones;
        }

        public async Task<bool> SaveAll()
        {
            return await context.SaveChangesAsync() > 0;
        }
    }
}