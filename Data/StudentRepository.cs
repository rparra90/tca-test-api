using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TCATest.API.Models;

namespace TCATest.API.Data
{
    public class StudentRepository : IStudentRepository
    {
        private readonly DataContext context;

        public StudentRepository(DataContext context)
        {
            this.context = context;
        }

        public void Add<T>(T entity) where T : class
        {
            this.context.Add(entity);
        }

        public void Delete<T>(T entity) where T : class
        {
            this.context.Remove(entity);
        }

        public async Task<Student> GetStudent(int id)
        {
            var student = await this.context.Students.FirstOrDefaultAsync(x => x.Id == id);
            return student;
        }

        public async Task<IEnumerable<Student>> GetStudents()
        {
            var students = await this.context.Students.ToListAsync();
            return students;
        }

        public async Task<bool> SaveAll()
        {
            return await this.context.SaveChangesAsync() > 0;
        }
    }
}