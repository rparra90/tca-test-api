using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TCATest.API.Models;

namespace TCATest.API.Data
{
    public class EmailRepository : IEmailRepository
    {
        private readonly DataContext context;

        public EmailRepository(DataContext context)
        {
            this.context = context;
        }

        public void Add<T>(T entity) where T : class
        {
            this.context.Add(entity);
        }

        public void Delete<T>(T entity) where T : class
        {
            this.context.Remove(entity);
        }

        public async Task<Email> GetEmail(int id)
        {
            var email = await this.context.Emails.FirstOrDefaultAsync(x => x.Id == id);
            return email;
        }

        public async Task<IEnumerable<Email>> GetEmails()
        {
            var emails = await this.context.Emails.ToListAsync();
            return emails;
        }

        public async Task<IEnumerable<Email>> GetEmailsWhereStudent(int id)
        {
            var emails = await this.context.Emails.Where(x => x.StudentId == id).ToListAsync();
            return emails;
        }

        public async Task<bool> SaveAll()
        {
            return await this.context.SaveChangesAsync() > 0;
        }
    }
}