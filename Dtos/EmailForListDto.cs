using System;

namespace TCATest.API.Dtos
{
    public class EmailForListDto
    {
        public int Id { get; set; }
        public int StudentId { get; set; }
        public string EmailString { get; set; }
        public string EmailType { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
    }
}