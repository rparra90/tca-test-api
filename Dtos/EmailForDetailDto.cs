using System;
using TCATest.API.Models;

namespace TCATest.API.Dtos
{
    public class EmailForDetailDto
    {
        public int Id { get; set; }
        public int StudentId { get; set; }
        public string EmailString { get; set; }
        public string EmailType { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; } 
    }
}