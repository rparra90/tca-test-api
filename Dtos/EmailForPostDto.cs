using System.ComponentModel.DataAnnotations;

namespace TCATest.API.Dtos
{
    public class EmailForPostDto
    {
        [Required]
        public int StudentId { get; set; }
        [Required]
        [EmailAddress]
        public string EmailString { get; set; }
        [Required]
        public string EmailType { get; set; }
    }
}