using System.ComponentModel.DataAnnotations;

namespace TCATest.API.Dtos
{
    public class AddressForPutDto
    {
        public int StudentId { get; set; }
        public string AddressLine { get; set; }
        public string City { get; set; }
        [RegularExpression("[0-9]*", ErrorMessage = "Zip code must be a number")]
        public string ZipPostcode { get; set; }
        public string State { get; set; } 
    }
}