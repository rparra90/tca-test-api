using System.ComponentModel.DataAnnotations;

namespace TCATest.API.Dtos
{
    public class PhoneForPostDto
    {
        [Required]
        public int StudentId { get; set; }
        [Required]
        [Phone(ErrorMessage = "A phone number is required")]
        public string PhoneNumber { get; set; }
        [Required]
        [RegularExpression("[0-9]*", ErrorMessage = "An area code must be only numbers")]
        public string CountryCode { get; set; }
        [Required]
        [RegularExpression("[0-9]*", ErrorMessage = "A country code must be only numbers")]
        public string AreaCode { get; set; }
        [Required]
        public string PhoneType { get; set; }
    }
}