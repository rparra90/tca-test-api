namespace TCATest.API.Dtos
{
    public class AddressForDetailDto
    {
        public int Id { get; set; }
        public int StudentId { get; set; }
        public string AddressLine { get; set; }
        public string City { get; set; }
        public string ZipPostcode { get; set; }
        public string State { get; set; } 
    }
}