using System.ComponentModel.DataAnnotations;

namespace TCATest.API.Dtos
{
    public class PhoneForPutDto
    {
        public int StudentId { get; set; }
        [Phone(ErrorMessage = "A phone number is required")]
        public string PhoneNumber { get; set; }
        [RegularExpression("[0-9]*", ErrorMessage = "An area code must be only numbers")]
        public string AreaCode { get; set; }
        [RegularExpression("[0-9]*", ErrorMessage = "A country code must be only numbers")]
        public string CountryCode { get; set; }
        public string PhoneType { get; set; }
    }
}