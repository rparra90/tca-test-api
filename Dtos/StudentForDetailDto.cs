using System.Collections;
using System.Collections.Generic;
using TCATest.API.Models;

namespace TCATest.API.Dtos
{
    public class StudentForDetailDto
    {
        public int Id { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string FirstName { get; set; }
        public string Gender { get; set; }
        public IEnumerable Addresses { get; set; }
        public IEnumerable Emails { get; set; }
        public IEnumerable Phones { get; set; }

    }
}