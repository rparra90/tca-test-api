using System.ComponentModel.DataAnnotations;

namespace TCATest.API.Dtos
{
    public class EmailForPutDto
    {
        public int StudentId { get; set; }
        [EmailAddress]
        public string EmailString { get; set; }
        public string EmailType { get; set; }
    }
}