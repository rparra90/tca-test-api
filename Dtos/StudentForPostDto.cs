using System.ComponentModel.DataAnnotations;

namespace TCATest.API.Dtos
{
    public class StudentForPostDto
    {
        [Required(ErrorMessage = "Last name is required")]
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        [Required(ErrorMessage = "First name is required")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Gender is required")]
        public string Gender { get; set; }
    }
}