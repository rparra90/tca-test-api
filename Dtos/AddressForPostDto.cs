using System.ComponentModel.DataAnnotations;

namespace TCATest.API.Dtos
{
    public class AddressForPostDto
    {
        [Required]
        public int StudentId { get; set; }
        [Required]
        public string AddressLine { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        [RegularExpression("[0-9]*", ErrorMessage = "Zip code must be a number")]
        public string ZipPostcode { get; set; }
        [Required]
        public string State { get; set; }
    }
}