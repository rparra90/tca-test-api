using System.ComponentModel.DataAnnotations;

namespace TCATest.API.Dtos
{
    public class StudentForPutDto
    {
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string FirstName { get; set; }
        public string Gender { get; set; }
    }
}