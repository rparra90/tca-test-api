using System;

namespace TCATest.API.Dtos
{
    public class PhoneForListDto
    {
        public int Id { get; set; }
        public int StudentId { get; set; }
        public string PhoneNumber { get; set; }
        public string PhoneType { get; set; }
        public string AreaCode { get; set; }
        public string CountryCode { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
    }
}