using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using TCATest.API.Data;
using TCATest.API.Dtos;
using TCATest.API.Models;
using TCATest.API.Utils;

namespace TCATest.API.Controllers
{
    [Route("api/[controller]")]
    public class AddressesController : Controller
    {
        private readonly IAddressRepository addressRepository;
        private readonly IStudentRepository studentRepository;
        private readonly IMapper mapper;

        public AddressesController(IAddressRepository addressRepository,
                                   IStudentRepository studentRepository,
                                   IMapper mapper)
        {
            this.addressRepository = addressRepository;
            this.studentRepository = studentRepository;
            this.mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetAddresses()
        {
            var addresses = await this.addressRepository.GetAddresses();
            var addressesToReturn = this.mapper.Map<IEnumerable<AddressForListDto>>(addresses);
            return Ok(addressesToReturn);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAddress(int id)
        {
            var address = await this.addressRepository.GetAddress(id);
            if (address == null)
                return NotFound(ApiMessages.NOT_FOUND);

            var addressToReturn = this.mapper.Map<AddressForDetailDto>(address);
            
            return Ok(addressToReturn);
        }

        [HttpPost]
        public async Task<IActionResult> PostAddress([FromBody] AddressForPostDto addressForPost)
        {
            var studentAddress = await this.studentRepository.GetStudent(addressForPost.StudentId);
            if (studentAddress == null)
                ModelState.AddModelError("Student", "Student not found");
            
            var addressToCreate = new Address();
            addressToCreate.StudentId = addressForPost.StudentId;
            addressToCreate.AddressLine = addressForPost.AddressLine;
            addressToCreate.City = addressForPost.City;
            addressToCreate.ZipPostcode = addressForPost.ZipPostcode;
            addressToCreate.State = addressForPost.State;

            var addressToReturn = this.mapper.Map<AddressForPostDto>(addressToCreate);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            this.addressRepository.Add(addressToCreate);
            await this.addressRepository.SaveAll();

            return Ok(addressToReturn);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutAddress(int id, [FromBody] AddressForPutDto addressForPut)
        {
            var addressToEdit = await this.addressRepository.GetAddress(id);
            if (addressToEdit == null)
                ModelState.AddModelError("Address", "Address not found");

            if (addressForPut.StudentId != 0)
            {
                var studentAddress = await this.studentRepository.GetStudent(addressForPut.StudentId);
                if (studentAddress == null)
                    ModelState.AddModelError("Student", "Student not found");

                addressToEdit.StudentId = addressForPut.StudentId;
            }

            if (addressForPut.AddressLine != null && addressForPut.AddressLine != "")
                addressToEdit.AddressLine = addressForPut.AddressLine;

            if (addressForPut.City != null && addressForPut.City != "")
                addressToEdit.City = addressForPut.City;

            if (addressForPut.ZipPostcode != null && addressForPut.ZipPostcode != "")
                addressToEdit.ZipPostcode = addressForPut.ZipPostcode;

            if (addressForPut.State != null && addressForPut.State != "")
                addressToEdit.State = addressForPut.State;

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            await this.addressRepository.SaveAll();

            var addressToReturn = this.mapper.Map<AddressForPutDto>(addressToEdit);

            return Ok(addressToReturn);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAddress(int id)
        {
            var addressToDelete = await this.addressRepository.GetAddress(id);
            if (addressToDelete == null)
                ModelState.AddModelError("Address", "Address not found");

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            this.addressRepository.Delete(addressToDelete);
            await this.addressRepository.SaveAll();

            return Ok(ApiMessages.ITEM_DELETED);
        }
    }
}