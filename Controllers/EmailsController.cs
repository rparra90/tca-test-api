using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using TCATest.API.Data;
using TCATest.API.Dtos;
using TCATest.API.Models;
using TCATest.API.Utils;

namespace TCATest.API.Controllers
{
    [Route("api/[controller]")]
    public class EmailsController : Controller
    {
        private readonly IEmailRepository emailRepository;
        private readonly IStudentRepository studentRepository;
        private readonly IMapper mapper;

        public EmailsController(IMapper mapper,
                                IEmailRepository emailRepository,
                                IStudentRepository studentRepository)
        {
            this.mapper = mapper;
            this.emailRepository = emailRepository;
            this.studentRepository = studentRepository;
        }

        [HttpGet]
        public async Task<IActionResult> GetEmails()
        {
            var emails = await this.emailRepository.GetEmails();
            var emailsToReturn = this.mapper.Map<IEnumerable<EmailForListDto>>(emails);
            return Ok(emailsToReturn);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetEmail(int id)
        {
            var email = await this.emailRepository.GetEmail(id);
            if (email == null)
                return NotFound(ApiMessages.NOT_FOUND);

            var emailToReturn = this.mapper.Map<EmailForDetailDto>(email);
            return Ok(emailToReturn);
        }

        [HttpPost]
        public async Task<IActionResult> PostEmail([FromBody] EmailForPostDto emailForPost)
        {
            var studentEmail = await this.studentRepository.GetStudent(emailForPost.StudentId);
            if (studentEmail == null)
                ModelState.AddModelError("Student", "Student not found");

            var emailToCreate = new Email();
            emailToCreate.StudentId = emailForPost.StudentId;
            emailToCreate.EmailString = emailForPost.EmailString;

            try {
                emailToCreate.EmailType = (EmailTypeEnum) Enum.Parse(typeof(EmailTypeEnum), emailForPost.EmailType);
            } catch (Exception e) {
                ModelState.AddModelError("Email Type", "Wrong email type");
            }

            var emailToReturn = this.mapper.Map<EmailForDetailDto>(emailToCreate);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            this.emailRepository.Add(emailToCreate);
            await this.emailRepository.SaveAll();

            return Ok(emailToReturn);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutEmail(int id, [FromBody] EmailForPutDto emailForPut)
        {
            var emailToEdit = await this.emailRepository.GetEmail(id);
            if (emailToEdit == null)
                ModelState.AddModelError("Email", "Email not found");

            emailToEdit.UpdatedOn = DateTime.Now;

            if (emailForPut.StudentId != 0)
            {
                var studentEmail = await this.studentRepository.GetStudent(emailForPut.StudentId);
                if (studentEmail == null)
                    return NotFound(ApiMessages.NOT_FOUND);
                
                emailToEdit.StudentId = emailForPut.StudentId;
            }

            if (emailForPut.EmailString != null && emailForPut.EmailString != "")
                emailToEdit.EmailString = emailForPut.EmailString;

            if (emailForPut.EmailType != null && emailForPut.EmailType != "")
            {
                try {
                    emailToEdit.EmailType = (EmailTypeEnum) Enum.Parse(typeof(EmailTypeEnum), emailForPut.EmailType);
                } catch (Exception e) {
                    ModelState.AddModelError("Email type", "Wrong email type");
                }
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            await this.emailRepository.SaveAll();

            var emailToReturn = this.mapper.Map<EmailForDetailDto>(emailToEdit);

            return Ok(emailToReturn);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteEmail(int id)
        {
            var emailToDelete = await this.emailRepository.GetEmail(id);
            if (emailToDelete == null)
                return NotFound("Email not found");

            this.emailRepository.Delete(emailToDelete);
            await this.emailRepository.SaveAll();

            return Ok(ApiMessages.ITEM_DELETED);
        }
    }
}