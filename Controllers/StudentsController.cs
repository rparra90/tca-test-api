using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using TCATest.API.Data;
using TCATest.API.Dtos;
using TCATest.API.Models;
using TCATest.API.Utils;

namespace TCATest.API.Controllers
{
    [Route("api/[controller]")]
    public class StudentsController : Controller
    {
        private readonly IStudentRepository studentRepository; 
        private readonly IEmailRepository emailRepository;
        private readonly IPhoneRepository phoneRepository;
        private readonly IAddressRepository addressRepository;
        private readonly IMapper mapper;

        public StudentsController(IStudentRepository studentRepository, 
                                 IEmailRepository emailRepository,
                                 IPhoneRepository phoneRepository,
                                 IAddressRepository addressRepository,
                                 IMapper mapper)
        {
            this.studentRepository = studentRepository;
            this.emailRepository = emailRepository;
            this.phoneRepository = phoneRepository;
            this.addressRepository = addressRepository;
            this.mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetStudents()
        {
            var students = await this.studentRepository.GetStudents();
            var studentsToReturn = this.mapper.Map<IEnumerable<StudentForListDto>>(students);

            return Ok(studentsToReturn);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetStudent(int id)
        {
            var student = await this.studentRepository.GetStudent(id);
            if (student == null)
                return NotFound(ApiMessages.NOT_FOUND);

            var studentToReturn = this.mapper.Map<StudentForDetailDto>(student);

            var studentEmails = await this.emailRepository.GetEmailsWhereStudent(id);
            if (studentEmails != null)
                studentToReturn.Emails = this.mapper.Map<IEnumerable<EmailForListDto>>(studentEmails);

            var studentPhones = await this.phoneRepository.GetPhonesWhereStudent(id);
            if (studentPhones != null)
                studentToReturn.Phones = this.mapper.Map<IEnumerable<PhoneForListDto>>(studentPhones);

            var studentAddresses = await this.addressRepository.GetAddressesWhereStudent(id);
            if (studentAddresses != null)
                studentToReturn.Addresses = this.mapper.Map<IEnumerable<AddressForListDto>>(studentAddresses);

            return Ok(studentToReturn);
        }

        [HttpPost]
        public async Task<IActionResult> PostStudent([FromBody] StudentForPostDto studentForPost)
        {
            var studentToCreate = new Student();

            studentToCreate.LastName = studentForPost.LastName;
            studentToCreate.MiddleName = studentForPost.MiddleName;
            studentToCreate.FirstName = studentForPost.FirstName;

            try {
                studentToCreate.Gender = (GenderEnum) Enum.Parse(typeof(GenderEnum), studentForPost.Gender);
            } catch (Exception e) {
                ModelState.AddModelError("Gender type", "Wrong gender type");
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            this.studentRepository.Add(studentToCreate);
            await this.studentRepository.SaveAll();

            var studentToReturn = this.mapper.Map<StudentForPostDto>(studentToCreate);

            return Ok(studentToReturn);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutStudent(int id, [FromBody] StudentForPutDto studentForPut)
        {
            var studentToEdit = await this.studentRepository.GetStudent(id);

            if (studentToEdit == null)
                ModelState.AddModelError("Student", "Student not found");

            studentToEdit.UpdatedOn = DateTime.Now;

            if (studentForPut.LastName != null && studentForPut.LastName != "")
                studentToEdit.LastName = studentForPut.LastName;

            if (studentForPut.MiddleName != null && studentForPut.MiddleName != "")
                studentToEdit.MiddleName = studentForPut.MiddleName;

            if (studentForPut.FirstName != null && studentForPut.FirstName != "")
                studentToEdit.FirstName = studentForPut.FirstName;

            if (studentForPut.Gender != null && studentForPut.Gender != "")
            {
                try {
                    studentToEdit.Gender = (GenderEnum) Enum.Parse(typeof(GenderEnum), studentForPut.Gender);
                } catch (Exception e) {
                    ModelState.AddModelError("Gender type", "Wrong gender type");
                }
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            await this.studentRepository.SaveAll();
            var studentToReturn = this.mapper.Map<StudentForPutDto>(studentToEdit);

            return Ok(studentToReturn);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteStudent(int id)
        {
            var studentToDelete = await this.studentRepository.GetStudent(id);
            if (studentToDelete == null)
                return NotFound(ApiMessages.NOT_FOUND);
            
            this.studentRepository.Delete(studentToDelete);
            await this.studentRepository.SaveAll();

            return Ok(ApiMessages.ITEM_DELETED);
        }
    }
}