using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using TCATest.API.Data;
using TCATest.API.Dtos;
using TCATest.API.Models;
using TCATest.API.Utils;

namespace TCATest.API.Controllers
{
    [Route("api/[controller]")]
    public class PhonesController : Controller
    {
        private readonly IPhoneRepository phoneRepository;
        private readonly IStudentRepository studentRepository;
        private readonly IMapper mapper;

        public PhonesController(IPhoneRepository phoneRepository,
                                IStudentRepository studentRepository,
                                IMapper mapper)
        {
            this.phoneRepository = phoneRepository;
            this.studentRepository = studentRepository;
            this.mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetPhones()
        {
            var phones = await this.phoneRepository.GetPhones();
            var phonesToReturn = this.mapper.Map<IEnumerable<PhoneForListDto>>(phones);
            return Ok(phonesToReturn);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetPhone(int id)
        {
            var phone = await this.phoneRepository.GetPhone(id);
            if (phone == null)
                return NotFound(ApiMessages.NOT_FOUND);

            var phoneToReturn = this.mapper.Map<PhoneForDetailDto>(phone);
            return Ok(phoneToReturn);
        }

        [HttpPost]
        public async Task<IActionResult> PostPhone([FromBody] PhoneForPostDto phoneForPost)
        {
            var studentPhone = await this.studentRepository.GetStudent(phoneForPost.StudentId);
            if (studentPhone == null)
                ModelState.AddModelError("Student", "Student not found");

            var phoneToCreate = new Phone();
            phoneToCreate.StudentId = phoneForPost.StudentId;
            phoneToCreate.PhoneNumber = phoneForPost.PhoneNumber;
            phoneToCreate.CountryCode = phoneForPost.CountryCode;
            phoneToCreate.AreaCode = phoneForPost.AreaCode;

            try {
                phoneToCreate.PhoneType = (PhoneTypeEnum) Enum.Parse(typeof(PhoneTypeEnum), phoneForPost.PhoneType);
            } catch (Exception e) {
                ModelState.AddModelError("PhoneType", "Wrong phone type");
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            this.phoneRepository.Add(phoneToCreate);
            await this.phoneRepository.SaveAll();

            var phoneToReturn = this.mapper.Map<PhoneForPostDto>(phoneToCreate);
            return Ok(phoneToReturn);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutPhone(int id, [FromBody] PhoneForPutDto phoneForPut)
        {
            var phoneToEdit = await this.phoneRepository.GetPhone(id);
            if (phoneToEdit == null)
                ModelState.AddModelError("Student", "Student not found");
            
            phoneToEdit.UpdatedOn = DateTime.Now;

            if (phoneForPut.StudentId != 0)
            {
                var studentPhone = await this.studentRepository.GetStudent(phoneForPut.StudentId);
                if (studentPhone == null)
                    return NotFound(ApiMessages.NOT_FOUND);

                phoneToEdit.StudentId = phoneForPut.StudentId;
            }

            if (phoneForPut.PhoneNumber != null && phoneForPut.PhoneNumber != "")
                phoneToEdit.PhoneNumber = phoneForPut.PhoneNumber;

            if (phoneForPut.AreaCode != null && phoneForPut.AreaCode != "")
                phoneToEdit.AreaCode = phoneForPut.AreaCode;

            if (phoneForPut.CountryCode != null && phoneForPut.CountryCode != "")
                phoneToEdit.CountryCode = phoneForPut.CountryCode;
            
            if (phoneForPut.PhoneType != null && phoneForPut.PhoneType != "")
            {
                try {
                    phoneToEdit.PhoneType = (PhoneTypeEnum) Enum.Parse(typeof(PhoneTypeEnum), phoneForPut.PhoneType);
                } catch (Exception e) {
                    ModelState.AddModelError("PhoneType", "Wrong phone type");
                }
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            await this.phoneRepository.SaveAll();
            
            var phoneToReturn = this.mapper.Map<PhoneForPutDto>(phoneToEdit);
            return Ok(phoneToReturn);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePhone(int id)
        {
            var phoneToDelete = await this.phoneRepository.GetPhone(id);
            if (phoneToDelete == null)
                return NotFound(ApiMessages.NOT_FOUND);
            
            this.phoneRepository.Delete(phoneToDelete);
            await this.phoneRepository.SaveAll();

            return Ok(ApiMessages.ITEM_DELETED);
        }
    }
}