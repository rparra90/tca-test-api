using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TCATest.API.Models
{
    public class Student
    {
        public int Id { get; set; }

        [StringLength(45)]
        public string LastName { get; set; }

        [StringLength(45)]
        public string MiddleName { get; set; }

        [StringLength(45)]
        public string FirstName { get; set; }

        public GenderEnum Gender { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime UpdatedOn { get; set; }

        public ICollection<Email> Emails { get; set; }
        public ICollection<Address> Addresses { get; set; }
        public ICollection<Phone> Phones { get; set; }

        public Student()
        {
            this.CreatedOn = DateTime.Now;
            this.UpdatedOn = DateTime.Now;
        }
    }
}