using System.ComponentModel.DataAnnotations;

namespace TCATest.API.Models
{
    public class Address
    {
        public int Id { get; set; }

        public int StudentId { get; set; }

        public Student Student { get; set; }

        [StringLength(100)]
        public string AddressLine { get; set; }

        [StringLength(45)]
        public string City { get; set; }

        [StringLength(45)]
        public string ZipPostcode { get; set; }

        [StringLength(45)]
        public string State { get; set; }
    }
}