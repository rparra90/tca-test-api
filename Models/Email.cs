using System;
using System.ComponentModel.DataAnnotations;

namespace TCATest.API.Models
{
    public class Email
    {
        public int Id { get; set; }

        public int StudentId { get; set; }

        public Student Student { get; set; }

        [StringLength(100)]
        public String EmailString { get; set; }

        public EmailTypeEnum EmailType { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime UpdatedOn { get; set; }

        public Email()
        {
            this.CreatedOn = DateTime.Now;
            this.UpdatedOn = DateTime.Now;
        }
    }
}