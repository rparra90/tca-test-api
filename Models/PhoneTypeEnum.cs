namespace TCATest.API.Models
{
    public enum PhoneTypeEnum
    {
        other,
        home,
        work,
        mobile
    }
}