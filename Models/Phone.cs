using System;
using System.ComponentModel.DataAnnotations;

namespace TCATest.API.Models
{
    public class Phone
    {
        public int Id { get; set; }

        public int StudentId { get; set; }

        public Student Student { get; set; }

        [StringLength(30)]
        public string PhoneNumber { get; set; }

        public PhoneTypeEnum PhoneType { get; set; }

        [StringLength(5)]
        public string CountryCode { get; set; }

        [StringLength(5)]
        public string AreaCode { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime UpdatedOn { get; set; }

        public Phone()
        {
            this.CreatedOn = DateTime.Now;
            this.UpdatedOn = DateTime.Now;
        }
    }
}